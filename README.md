# Cosign Public Key Repository

The repository stores the [Cosign](https://github.com/sigstore/cosign) public key of images signed on registries:

- [Dockerhub](https://hub.docker.com/u/jfxs)
- [Quay.io](https://quay.io/user/jfx/)

Published on January 7, 2023.

## Signature and attestation

[Cosign](https://github.com/sigstore/cosign) public key:

```shell
-----BEGIN PUBLIC KEY-----
MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEa3yV6+yd/l4zh/tfT6Tx+zn0dhy3
BhFqSad1norLeKSCN2MILv4fZ9GA6ODOlJOw+7vzUvzZVr9IXnxEdjoWJw==
-----END PUBLIC KEY-----
```

The public key is also available online: <https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub>.

```shell
curl --progress-bar -o cosign.pub https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub
```

or

```shell
wget -cq --show-progress -O cosign.pub https://gitlab.com/op_so/docker/cosign-public-key/-/raw/main/cosign.pub
```

To verify an image:

```shell
cosign verify --key cosign.pub $IMAGE_URI
```

To verify and get the SBOM attestation:

```shell
cosign verify-attestation --key cosign.pub --type spdxjson $IMAGE_URI | jq '.payload | @base64d | fromjson | .predicate'
```

## Authors

- **FX Soubirou** - *Initial work* - [GitLab repositories](https://gitlab.com/op_so)
